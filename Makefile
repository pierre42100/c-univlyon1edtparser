CC=gcc
CFLAGS= -Wall
LIBS=
DEPS=
OBJ= main.o edt_parser.o

all: clean edt_parser

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

edt_parser: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -rf *.o *.a *.exe edt_parser

run: all
	./edt_parser

install: all
	cp ./edt_parser /usr/bin/edt

uninstall:
	rm -f /usr/bin/edt