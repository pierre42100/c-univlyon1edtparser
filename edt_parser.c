#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "edt_parser.h"


/**
 * Calendar retrievement URL
 *
 * Sample URL (G5S1 - 2018) :  http://adelb.univ-lyon1.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=41580&projectId=2&calType=ical&firstDate=2018-01-01&lastDate=2019-06-15
 */
#define CALENDAR_DOMAIN "adelb.univ-lyon1.fr"
#define CALENDAR_PORT 80
#define CALENDAR_URI "/jsp/custom/modules/plannings/anonymous_cal.jsp?projectId=4&calType=ical&firstDate=%s&lastDate=%s&resources=%d"

#define REPLY_READ_PACKET_SIZE 1000

#define PARSE_ERROR(msg) {fprintf(stderr, "Parse error: %s\n", msg); return NULL;}
#define MSG_VERBOSE(msg) {printf("Verbose: %s\n", msg);}
#define MALLOC(var, size){var = malloc(size); if(var==NULL) PARSE_ERROR("Could not allocate memory!");}

static int parseEvent(struct edt_parser_event_t *event, char *info);

void edt_parser_get_request_date_from_now(char *date, int num_days) {

	//Get the current date for the parser
	time_t curr_time = time(NULL) + num_days*3600*24;
	struct tm *local_time = localtime(&curr_time);
	strftime(date, 12, "%F", local_time);
}

struct edt_parser_event_t* edt_parser_parse(int res_id, char *begin_date, char *end_date) {

	//First, we determine the URL
	if(res_id < 1)
		PARSE_ERROR("Calendar ressource ID specified is invalid!");

	//Get server hostname
	struct hostent *server_hostname;
	server_hostname = gethostbyname(CALENDAR_DOMAIN);
	if(server_hostname == NULL)
		PARSE_ERROR("Could not find hostname" CALENDAR_DOMAIN " !");

	//Open socket
	int socket_conn = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_conn < 0)
		PARSE_ERROR("Could not open a socket to retrieve calendar!");

	//Specify server information
	struct sockaddr_in server;
	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_port = htons(80);
	memcpy(&server.sin_addr.s_addr, server_hostname->h_addr, server_hostname->h_length);

	//Connect to remote server
	if(connect(socket_conn, (struct sockaddr *)&server, sizeof(server)) < 0)
		PARSE_ERROR("Could not connect to server!");

	//Notify client
	MSG_VERBOSE("Connected to "CALENDAR_DOMAIN);

	//Create the request to send to the server
	char request[300];
	sprintf(request, 
		"GET "CALENDAR_URI" HTTP/1.1\r\nHost: "CALENDAR_DOMAIN"\r\nUser-Agent: EdtParserByPierre1.0\r\n\r\n", 
		begin_date, end_date, res_id);
	
	//Send the request
	MSG_VERBOSE("Sending request to calendar server");
	if(send(socket_conn, request, strlen(request), 0) < 0)
		PARSE_ERROR("Could not send request to server!");

	//Receive server reply
	char begin_reply[REPLY_READ_PACKET_SIZE];
	if(read(socket_conn, begin_reply, REPLY_READ_PACKET_SIZE) < 0)
		PARSE_ERROR("Could not start to receive server reply!");

	//Check the server did not return an error
	if(strstr(begin_reply, "HTTP/1.1 200") == NULL){
		MSG_VERBOSE(begin_reply);
		PARSE_ERROR("The server replied an error!");
	}

	//Check if the begining of the calendar is included in the response
	char *begin_calendar = strstr(begin_reply, "BEGIN:VCALENDAR");
	if(begin_calendar == NULL)
		PARSE_ERROR("Could not find the begining of the calendar!");

	//Extract server response
	char *server_response = NULL;
	MALLOC(server_response, sizeof(char)*(strlen(begin_calendar)+1));
	strcpy(server_response, begin_calendar);


	//Receive data until the server finish to deliver content
	while(strstr(server_response, "END:VCALENDAR") == NULL){

		
		//Prepare to receive response
		char *new_response = NULL;
		MALLOC(new_response, sizeof(char)*(strlen(server_response) + REPLY_READ_PACKET_SIZE + 1));

		//Copy previous response
		strcpy(new_response, server_response);

		//Receive more content
		char *begin_write = strchr(new_response, '\0');
		int bytes_read = read(socket_conn, begin_write, REPLY_READ_PACKET_SIZE);
		if(bytes_read < 0)
			PARSE_ERROR("Could not receive completely server reply!");

		//Close string
		*(begin_write + bytes_read) = '\0';

		//Swap response
		free(server_response);
		server_response = new_response;

	}

	//Close socket
	close(socket_conn);
	MSG_VERBOSE("Disconnected from "CALENDAR_DOMAIN);

	//Now it is time to parse calendar
	char *curr_pos = server_response;
	struct edt_parser_event_t *previous_event = NULL;
	struct edt_parser_event_t *curr_event = NULL;

	while(strstr(curr_pos, "BEGIN:VEVENT") != NULL){

		//Parse event
		curr_event = NULL;
		MALLOC(curr_event, sizeof(struct edt_parser_event_t));
		if(parseEvent(curr_event, strstr(curr_pos, "BEGIN:VEVENT")) < 0)
			PARSE_ERROR("Could not parse information about an event!");

		//Year, I know, it reminds you the 90's but it is so easy to do...
		curr_event->previous = previous_event;
		previous_event = curr_event;

		//Go to next entry
		curr_pos = strstr(curr_pos, "BEGIN:VEVENT") + 5;
	}

	MSG_VERBOSE("Finished parsing events");

	//Free memory
	free(server_response);

	return previous_event;
}

static int ipow(int number, int count){
	int result = 1;

	while(count > 0){
		result *= number;
		count--;
	}

	return result;
}

static int natoi(char *string, int length){

	int number = 0;

	for(int i = 0; i < length; i++){
		number += (string[i] - '0')*ipow(10, length-i-1);
	}

	return number;

}

static void parseDate(struct edt_parser_date_t *date, char *str){
	date->year = natoi(str, 4);
	date->month = natoi(str+4, 2);
	date->day = natoi(str+6, 2);
	date->hour = natoi(str+9, 2) + 2;
	date->minute = natoi(str+11, 2);
	date->second = natoi(str+13, 2);

	//Include hour change of FRANCE
	if(date->year < 2018)
		;//Do nothing : no supported and useless
	else if(date->year == 2018){
		date->hour--; //This code was made on november, 2018
	}
	else if(date->year == 2019){
		if(date->month < 3 || (date->month == 3 && date->day < 30)
			|| date->month > 10 || (date->month == 10 && date->day > 26))
			date->hour--; // Winter hour
	}
	else if(date->year == 2020){
		if(date->month < 3 || (date->month == 3 && date->day < 28)
			|| date->month > 10 || (date->month == 10 && date->day > 24))
			date->hour--; // Winter hour
	}
	else if(date->year == 2021){
		if(date->month < 3 || (date->month == 3 && date->day < 27)
			|| date->month > 10 || (date->month == 10 && date->day > 30))
			date->hour--; // Winter hour
	}

	//Too broad in the future
	else {
		fprintf(stderr, "edt_parser do not support dates (y=%d) above 2021!!!", date->year);
		exit(EXIT_FAILURE);
	}
	
}

static void parseString(char **dest, char *src, int length){
	*dest = malloc(sizeof(char)*(length +1));

	if(*dest == NULL){
		MSG_VERBOSE("Could not allocate memory for a string!");
		exit(EXIT_FAILURE);
	}

	strcpy(*dest, src);
}

static int parseEvent(struct edt_parser_event_t *event, char *info) {
	
	char *curr_pos = info;
	char *end = strstr(curr_pos, "END:VEVENT");
	if(end == NULL){
		MSG_VERBOSE("Unterminated event!");
		return -1;
	}

	//For security purpose, we clear some information
	event->summary = NULL;
	event->location = NULL;
	event->description = NULL;
	event->uid = NULL;

	//We need to parse event line per line
	char *two_points, *line_break;
	char *begin_key, *begin_value;
	int key_length, value_length;
	char *key, *value = NULL;
	while(curr_pos < end){

		two_points = strchr(curr_pos, ':');
		line_break = strchr(two_points, '\r');

		//Search for appropriate line break
		while(*(line_break +2) == ' ' && *(line_break +3) == ' '){
			line_break = strchr(line_break + 1, '\r');
		}

		if(two_points == NULL || line_break == NULL || two_points > line_break){
			MSG_VERBOSE("Warning : invalid event format detected!");
			return -1;
		}

		//Current positions
		begin_key = curr_pos;
		key_length = two_points - begin_key;
		begin_value = two_points + 1;
		value_length = line_break - begin_value;

		//Allocate memory
		key = NULL;
		value = NULL;
		key = malloc(sizeof(char)*(key_length + 1));
		value = malloc(sizeof(char)*(value_length + 1));

		if(key == NULL || value == NULL){
			MSG_VERBOSE("Could not allocate memory for key / value pair");
			return -1;
		}

		//Copy correctly strings
		strncpy(key, begin_key, key_length);
		strncpy(value, begin_value, value_length);
		key[key_length] = '\0';
		value[value_length] = '\0';

		//Process value
		if(strcmp(key, "DTSTART") == 0){
			parseDate(&event->date_start, value);
		}
		else if(strcmp(key, "DTEND") == 0){
			parseDate(&event->date_end, value);
		}
		else if(strcmp(key, "SUMMARY") == 0){
			parseString(&event->summary, value, value_length);
		}
		else if(strcmp(key, "LOCATION") == 0){
			parseString(&event->location, value, value_length);
		}
		else if(strcmp(key, "DESCRIPTION") == 0){
			parseString(&event->description, value, value_length);
		}
		else if(strcmp(key, "UID") == 0){
			parseString(&event->uid, value, value_length);
		}
		else if(strcmp(key, "CREATED") == 0){
			parseDate(&event->date_created, value);
		}
		else if(strcmp(key, "LAST-MODIFIED") == 0){
			parseDate(&event->date_last_modified, value);
		}



		//Free memory
		free(key);
		free(value);

		curr_pos = line_break + 2;
	}

	//Success
	return 0;
}

int edt_parser_events_count(struct edt_parser_event_t *first) {
	int count = 0;

	struct edt_parser_event_t *curr = first;

	while(curr != NULL){
		count++;
		curr = curr->previous;
	}

	return count;
}

struct edt_parser_event_t *edt_parser_event_get_at(struct edt_parser_event_t *first, int count) {

	struct edt_parser_event_t *event = first;

	while(count > 0){
		event = event->previous;
		count--;
	}

	return event;

}

void swapEvents(struct edt_parser_event_t *event_1, struct edt_parser_event_t *event_2){

	struct edt_parser_event_t swap;
	swap = *event_1;
	*event_1 = *event_2;
	*event_2 = swap;

	event_2->previous = event_1->previous;
	event_1->previous = swap.previous;
}

int edt_parser_compares_dates(struct edt_parser_date_t *date_1, struct edt_parser_date_t *date_2){
	
	//Compares dates
	if(date_1->year < date_2->year)
		return -1;
	else if(date_1->year > date_2->year)
		return 1;

	if(date_1->month < date_2->month)
		return -1;
	else if(date_1->month > date_2->month)
		return 1;

	if(date_1->day < date_2->day)
		return -1;
	else if(date_1->day > date_2->day)
		return 1;

	if(date_1->hour < date_2->hour)
		return -1;
	else if(date_1->hour > date_2->hour)
		return 1;

	if(date_1->minute < date_2->minute)
		return -1;
	else if(date_1->minute > date_2->minute)
		return 1;

	if(date_1->second < date_2->second)
		return -1;
	else if(date_1->second > date_2->second)
		return 1;

	return 0;
}

void edt_parser_sort(struct edt_parser_event_t *event) {
	
	int count = edt_parser_events_count(event);

	struct edt_parser_event_t *event_1 = NULL;
	struct edt_parser_event_t *event_2 = NULL;

	struct edt_parser_event_t *previous_event = NULL;
	struct edt_parser_event_t *following_event = NULL;

	for(int i = 0; i < count; i++){

		for(int j = 1; j < count; j++){

			event_1 = edt_parser_event_get_at(event, j-1);
			event_2 = edt_parser_event_get_at(event, j);

			//Check if event_1 begins before event_2
			if(edt_parser_compares_dates(&event_1->date_start, &event_2->date_start) == 1){

				//Swap references of the two events

				//Get previous and following reference
				if(j == 1){
					//We must swap the internal content of the events
					swapEvents(event_1, event_2);
				}
				else {
					previous_event = edt_parser_event_get_at(event, j - 2);

					if(j == (count - 1))
						following_event = NULL;
					else
						following_event = edt_parser_event_get_at(event, j+1);

					previous_event->previous = event_2;
					event_2->previous = event_1;
					event_1->previous = following_event;
				}


			}

		}

	}
}

void edt_parser_free(struct edt_parser_event_t *event) {

	if(event->summary != NULL)
		free(event->summary);

	if(event->location != NULL)
		free(event->location);

	if(event->description != NULL)
		free(event->description);

	if(event->uid != NULL)
		free(event->uid);

	free(event);
}

void edt_parser_free_all(struct edt_parser_event_t *event) {
	struct edt_parser_event_t *curr_event = event;
	struct edt_parser_event_t *previous = NULL;

	while(curr_event != NULL){
		previous = curr_event->previous;
		edt_parser_free(curr_event);
		curr_event = previous;
	}
}
