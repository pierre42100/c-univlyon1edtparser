#include <stdio.h>
#include <stdlib.h>


#include "edt_parser.h"
#include "config.h"

int main(int argc, char **argv){

	printf("Simple edt parser. (c) Pierre HUBERT under the MIT License\n");

	int num_day = 0;

	if(argc > 1){
		sscanf(argv[1], "%d", &num_day);
	}

	//Get the date for the request
	char date[12];
	edt_parser_get_request_date_from_now(date, num_day);

	//Get the list of events
	struct edt_parser_event_t *first = edt_parser_parse(CALENDAR_ID, date, date);
	if(first == NULL){
		fprintf(stderr, "Could not parse calendar !!!!\n");
		exit(EXIT_FAILURE);
	}

	//Sort the list of events
	edt_parser_sort(first);

	printf("\n\n\nDate: %d/%02d/%02d\n\n\n",
		first->date_start.day,
		first->date_start.month,
		first->date_start.year);

	struct edt_parser_event_t *curr = first;
	while(curr != NULL){
		printf("%02d:%02d => %02d:%02d\n",
			curr->date_start.hour,
			curr->date_start.minute,
			curr->date_end.hour,
			curr->date_end.minute);

		if(curr->summary != NULL)
			printf(" Summary: %s\n", curr->summary);

		if(curr->location != NULL)
			printf(" Location: %s\n", curr->location);

		//Show other information only if requested
		if(SHOW_FULL_LIST > 0){

			if(curr->description != NULL)
				printf(" Description: %s\n", curr->description);

			if(curr->uid != NULL)
				printf(" UID: %s\n", curr->uid);

			printf(" Created: %d/%02d/%02d %02d:%02d:%02d\n", 
				curr->date_created.day,
				curr->date_created.month,
				curr->date_created.year, 
				curr->date_created.hour,
				curr->date_created.minute,
				curr->date_created.second);

			printf(" Updated: %d/%02d/%02d %02d:%02d:%02d\n", 
				curr->date_last_modified.day,
				curr->date_last_modified.month,
				curr->date_last_modified.year, 
				curr->date_last_modified.hour,
				curr->date_last_modified.minute,
				curr->date_last_modified.second);
		}

		printf("\n\n");

		curr = curr->previous;
	}

	//Clear memory
	edt_parser_free_all(first);

	return EXIT_SUCCESS;
}