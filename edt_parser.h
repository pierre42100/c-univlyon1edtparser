/**
 * Univ Lyon 1 EDT parser
 *
 * @author Pierre HUBERT
 */

#pragma once

struct edt_parser_date_t {
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
};

struct edt_parser_event_t {
	struct edt_parser_event_t *previous;

	//Event information
	struct edt_parser_date_t date_start;
	struct edt_parser_date_t date_end;
	char *summary;
	char *location;
	char *description;
	char *uid;
	struct edt_parser_date_t date_created;
	struct edt_parser_date_t date_last_modified;

};

/**
 * Get the date for the parse request, with a customizable number of day differences
 *
 * @param *date Output date (note: 11 characters are required for the date)
 * Mathis went here
 * @param num_day The number of days of difference
 */
void edt_parser_get_request_date_from_now(char *date, int num_days);

/**
 * Parse a given calendar URL into envent object and return pointer
 * on the first generated object. Note: it is the duty of the caller
 * to free memory when parsed data are not longer required
 *
 * @param res_id The ID of the calendar ressource
 * @param *begin_date The begin of retrievement interval
 * @param *end_date The end of the retrievement interval
 * @return Pointer on the first event or NULL object reference if an
 * error occurred.
 */
struct edt_parser_event_t* edt_parser_parse(int res_id, char *begin_date, char *end_date);

/**
 * Count the number of events present in a list
 *
 * @param first The first event to start to count
 * @return The number of events in the list
 */
int edt_parser_events_count(struct edt_parser_event_t *first);

/**
 * Retrieve the event located at a specified position
 *
 * @param *first The first event to start to count from
 * @param count The position in the list of the event to get
 * @return Pointer on event
 */
struct edt_parser_event_t *edt_parser_event_get_at(struct edt_parser_event_t *first, int count);

/**
 * Compares two dates
 * 
 * @param *date_1 The first date to compare
 * @param *date_2 The second date to compare
 * Returns 
 *   -1 if date_1 is earlier than date_2
 *    0 if the dates are the same
 *    1 if date_1 is older than date_2
 */
int edt_parser_compares_dates(struct edt_parser_date_t *date_1, struct edt_parser_date_t *date_2);

/**
 * Sort all the event following an event in ascending order
 *
 * @param *event The first event to sort
 */
void edt_parser_sort(struct edt_parser_event_t *event);

/**
 * Remove from memory a single entry
 *
 * @param *event The event to remove
 */
void edt_parser_free(struct edt_parser_event_t *event);

/**
 * Remove from memory a list of event
 *
 * Note : this function also delete events located
 * after this node
 *
 * @param *event The first event to remove
 */
void edt_parser_free_all(struct edt_parser_event_t *event);