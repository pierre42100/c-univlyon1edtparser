# UnivLyon1EdtParser

If you have to connect to edt.univ-lyon1.fr to get your work schedule, you will love this software! It is a command-line parser of the website.

## Requirements
This software is certified to run on Linux only. It make use of UNIX function. You may be able to build it for Windows using Cygwin...

## Configure
* Clone the project
* Update config.h to make it match your needs
* Run `make run` to make sure everything is going good.
* Run `sudo make install` to install the software system-wide.
* Everytime you need it, run `edt` from command line to get your schedule....

## Command-line usage
`edt [n]`
`n` is the number of day that separates the current date you want to get information about.
If `n` = 0, you will get the schedule of the current day, if `n` = 1, you will get the schedule of tomorrow, etc...
By default, `n` = 0.


## Project integration
If you want to reuse the parser, you just have to copy the files `edt_parser.h` and `edt_parser.c` into your project and learn how does the project works using `main.c`...

## Author and License
This sotware was built by Pierre HUBERT and is licensed under the MIT License