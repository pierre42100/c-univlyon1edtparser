/**
 * Project configuration
 *
 *
 * @author Pierre HUBERT
 */

/**
 * Calendar ressource ID
 *
 * Sample G5S1 (2018): 41580
 * G2S2 : 9304
 * G3S3 : 9313
 */
#define CALENDAR_ID 9313

/**
 * Specify whether full information list
 *  should be shown or not
 */
#define SHOW_FULL_LIST -1
